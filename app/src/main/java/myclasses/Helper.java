package myclasses;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.example.nmthi.fakemigrainebuddy.MainActivity;

/**
 * Created by nmthi on 3/11/2016.
 */

public class Helper {

    public static String PREFERENCES_FILE = "fmb";
    public static String PREF_USER_FIRST_TIME = "userFirstTime";

    public static String readSharedSetting(Context ctx, String settingName, String defaultValue) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        return sharedPref.getString(settingName, defaultValue);
    }
    public static void saveSharedSetting(Context ctx, String settingName, String settingValue) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(settingName, settingValue);
        editor.apply();
    }

    public static void goToMainActivityFirstTime(Activity activity) {
        saveSharedSetting(activity, Helper.PREF_USER_FIRST_TIME, "false");
        Intent main = new Intent(activity, MainActivity.class);
        main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(main);
        activity.finish();
    }
}
