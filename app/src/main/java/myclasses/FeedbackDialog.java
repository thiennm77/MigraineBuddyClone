package myclasses;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.example.nmthi.fakemigrainebuddy.R;

/**
 * Created by nmthi on 4/11/2016.
 */

public class FeedbackDialog extends Dialog implements View.OnClickListener {

    private Activity activity;

    public FeedbackDialog(Context context) {
        super(context);
        activity = (Activity) context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_feedback);

        findViewById(R.id.btnSuggest).setOnClickListener(this);
        findViewById(R.id.btnReport).setOnClickListener(this);
        findViewById(R.id.btnCancel).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        dismiss();
    }
}
