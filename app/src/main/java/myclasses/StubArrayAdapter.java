package myclasses;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.nmthi.fakemigrainebuddy.R;

/**
 * Created by nmthi on 4/11/2016.
 */

public class StubArrayAdapter extends ArrayAdapter {

    private Context context;
    private int layoutId;

    public StubArrayAdapter(Context context, int resource) {
        super(context, resource, new String[] { "" });
        this.context = context;
        layoutId = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(layoutId, parent, false);
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return "";
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
