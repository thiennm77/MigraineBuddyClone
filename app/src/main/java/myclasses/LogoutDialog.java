package myclasses;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.example.nmthi.fakemigrainebuddy.IntroActivity;
import com.example.nmthi.fakemigrainebuddy.R;

/**
 * Created by nmthi on 7/11/2016.
 */

public class LogoutDialog extends Dialog implements View.OnClickListener {

    private Activity activity;

    public LogoutDialog(Context context) {
        super(context);
        activity = (Activity) context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_logout);

        findViewById(R.id.btnBack).setOnClickListener(this);
        findViewById(R.id.btnLogout).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        dismiss();
        if (view.getId() == R.id.btnLogout) {
            Helper.saveSharedSetting(activity, Helper.PREF_USER_FIRST_TIME, "true");
            Intent intent = new Intent(activity, IntroActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(intent);
            activity.finish();
        }
    }
}
