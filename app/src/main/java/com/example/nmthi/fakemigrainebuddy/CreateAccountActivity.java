package com.example.nmthi.fakemigrainebuddy;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import myclasses.Helper;

public class CreateAccountActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        findViewById(R.id.btnSignUpFb).setOnClickListener(this);
        findViewById(R.id.btnSignUpGg).setOnClickListener(this);

        final EditText password = (EditText) findViewById(R.id.edtPassword);
        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    Helper.goToMainActivityFirstTime(CreateAccountActivity.this);
                    return true;
                }
                return false;
            }
        });

    }

    @Override
    public void onClick(View view) {
        Helper.goToMainActivityFirstTime(this);
    }
}
