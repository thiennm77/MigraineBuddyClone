package com.example.nmthi.fakemigrainebuddy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import myclasses.Helper;

public class LoginActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        findViewById(R.id.btnForgot).setOnClickListener(this);
        findViewById(R.id.btnLoginFb).setOnClickListener(this);
        findViewById(R.id.btnLoginGg).setOnClickListener(this);
        findViewById(R.id.btnNext).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnForgot:
                Intent intent = new Intent(this, ForgotPasswordActivity.class);
                startActivity(intent);
                break;
            default: // R.id.btnLoginFb, R.id.btnLoginGg, R.id.btnNext:
                Helper.goToMainActivityFirstTime(this);
        }
    }
}
