package com.example.nmthi.fakemigrainebuddy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class IntroActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

    }

    @Override
    public void onBackPressed() {
        int position = mViewPager.getCurrentItem();
        if (position == 0) {
            super.onBackPressed();
        } else {
            mViewPager.setCurrentItem(position-1);
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class IntroFragment extends Fragment {

        private static final String POSITION =  "position";

        private int[] introImageResId = { R.drawable.introduction_slide_image_0, R.drawable.introduction_slide_image_1,
                R.drawable.introduction_slide_image_2, R.drawable.introduction_slide_image_3 };

        public IntroFragment() {}

        public static IntroFragment newInstance(int sectionNumber) {
            IntroFragment fragment = new IntroFragment();
            Bundle args = new Bundle();
            args.putInt(POSITION, sectionNumber);
            fragment.setArguments(args);

            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView;
            int position = getArguments().getInt(POSITION);

            if (position == 4) {
                rootView = inflater.inflate(R.layout.fragment_intro, container, false);

                Button create = (Button) rootView.findViewById(R.id.btnCreate);
                create.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getContext(), CreateAccountActivity.class);
                        startActivity(intent);
                    }
                });

                Button haveAccount = (Button) rootView.findViewById(R.id.btnHave);
                haveAccount.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getContext(), LoginActivity.class);
                        startActivity(intent);
                    }
                });

            } else {
                rootView = inflater.inflate(R.layout.fragment_intro_image, container, false);
                ImageView imageView = (ImageView) rootView.findViewById(R.id.introImage);
                imageView.setImageResource(introImageResId[position]);
            }
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a IntroFragment (defined as a static inner class below).
            return IntroFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return 5;
        }


    }
}
