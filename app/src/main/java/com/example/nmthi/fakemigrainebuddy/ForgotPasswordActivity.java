package com.example.nmthi.fakemigrainebuddy;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ForgotPasswordActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        final Button send = (Button) findViewById(R.id.btnSend);

        final EditText email = (EditText) findViewById(R.id.edtEmail);
        email.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    if ("".equals(email.getText().toString())) {
                        send.setEnabled(false);
                    } else {
                        send.setEnabled(true);
                    }
                }
                return false;
            }
        });
    }
}
